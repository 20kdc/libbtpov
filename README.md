# BTPOV: Simple if limited GPU compute library

BTPOV is a library to make performing GPU compute tasks that don't particularly need integration with graphical tasks sane and simple.

BTPOV requires Vulkan. This and a functioning C standard library are it's only dependencies.

If targetting systems that may not have Vulkan, try OpenGL ES 2.

## Authors

Just 20kdc for now.

## Platforms

BTPOV has been tested on Linux with an AMD RAVEN2 APU with RADV drivers.

BTPOV has not been tested on Windows but should run there.

BTPOV has not been tested on Mac OS X... and may or may not run there.

## API Design

BTPOV API design has these tenets:

+ Always allow immediate commands. (Command buffer stuff might be added later, but it must always be optional.)
+ It is better to be simple than to be all-encompassing. Being all-encompassing and overtly flexible results in an API like Vulkan, and if you wanted that, you wouldn't be using this library.
+ *Don't break ABI.* (It is preferred regardless that BTPOV be used in the form of a static library, but not breaking ABI is still a good general idea.)
+ A corollary to "Don't break ABI": The only change that should be required to the examples, but particularly `examples/simple.c`, is to add more error-checking or to otherwise fix bugs in the examples. If these "bugs" were introduced via changes to BTPOV (i.e. created specifically by API changes, not an actual fault in the example) then the bug is with BTPOV, not the example.
+ It would be ideal if, when possible, direct access to Vulkan objects was discouraged in the hope of allowing some future effort towards multiple BTPOV backends.

## License

```
Copyright (c) 2020-2021 BTPOV contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

