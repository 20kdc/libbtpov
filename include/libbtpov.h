#ifndef LIBBTPOV_h_
#define LIBBTPOV_h_

#include <vulkan/vulkan.h>

// It is important to note that BTPOV does not automatically handle object lifetimes with respect to each other unless those objects are 'implied' by a utility function.
// In other words, if BTPOV automatically creates an object in the process of creating some other object, it will automatically destroy the 'implied' object.

// --- Error Reporting ---

// This is reserved for future use.
// It is also deliberately left abstract and honestly should remain abstract, with a family of BTPOV_CreateReporter functions and a BTPOV_DestroyReporter function.
// This, when introduced, allows error reporting APIs to be safely versioned.
// At this time, the BTPOV_Reporter * values passed to CreateInstance* family functions can and must always be NULL.
typedef struct BTPOV_Reporter BTPOV_Reporter;

// --- Instances ---

// Individual BTPOV Instances are thread-safe with respect to each other assuming that they do not share underlying resources that could make the API not thread-safe.
// However, calls within an Instance are not thread-safe.
typedef struct BTPOV_Instance BTPOV_Instance;

// Please see docs/apumem.md for the full situation as this appears to be something of an ongoing thing..
// Disables HSA check. The HSA check is designed to avoid using system memory unnecessarily on devices with unified memory.
#define BTPOV_CREATEINSTANCE_FLAGS_DONT_HSA_CHECK 0x1

// Standard init function. This will use an independent instance, and this will attempt to determine the best Vulkan device for running BTPOV on.
// It is usually best to use this function if the user has not specifically requested some override, as theoretically BTPOV may have been replaced with an emulating library.
// Returns null on error.
BTPOV_Instance * BTPOV_CreateInstanceAutomatic(uint32_t flags, BTPOV_Reporter * reporter);

// Semi-automatic init function. This allows explicitly choosing the physical device, and creating the instance yourself.
// But it still ensures the actual VkDevice is created and thus managed by BTPOV.
BTPOV_Instance * BTPOV_CreateInstanceVulkan(VkInstance instance, VkPhysicalDevice physDev, uint32_t flags, BTPOV_Reporter * reporter);

// Complex init function. This assumes you know what you're doing.
// Note that you are allowed to pass a fake VkPhysicalDeviceMemoryProperties if you need to "guide" BTPOV into making specific memory type choices.
// In this case, it is advisable to pass BTPOV_CREATEINSTANCE_FLAGS_DONT_HSA_CHECK, as if BTPOV "detects HSA" it will try to avoid using the heap that "looks like system memory".
// Returns null on error.
// In any case, the handles are assumed to be application-owned, and BTPOV will not delete them.
BTPOV_Instance * BTPOV_CreateInstanceVulkanExplicit(VkDevice device, VkQueue queue, VkCommandPool commandPool, const VkPhysicalDeviceProperties * info, const VkPhysicalDeviceMemoryProperties * memoryInfo, uint32_t flags, BTPOV_Reporter * reporter);

// Shuts down a BTPOV instance.
// You must manually destroy all objects associated with a BTPOV instance before destroying the instance itself.
// This function cannot error.
void BTPOV_DestroyInstance(BTPOV_Instance * instance);

// --- Buffers ---

typedef struct BTPOV_Buffer BTPOV_Buffer;

typedef enum {
	// Indicates the buffer will be written to by the GPU, and will be read by the CPU. (Therefore, this buffer should be kept in CPU memory.)
	// Note that this is not necessarily coherent or cached memory, as BTPOV will use whatever pools are available if it is unable to allocate memory from an optimal region.
	BTPOV_BUFFERMODE_CPU,
	// Indicates the buffer will be written to by the CPU, and will be read by the GPU. (Therefore, this buffer should be kept in GPU memory.)
	BTPOV_BUFFERMODE_GPU,
	// Indicates the buffer will only ever be accessed by the GPU. (Therefore, this buffer should be kept in GPU memory, and does not necessarily need to be available to the CPU.)
	BTPOV_BUFFERMODE_GPU_ONLY,
	// Not a valid mode value. Used internally. The range (0) ... (BTPOV_BUFFERMODE_MAX - 1) inclusive is always a valid list of buffer modes.
	BTPOV_BUFFERMODE_MAX
} BTPOV_BufferMode;

// Creates a buffer (uninitalized).
// Returns null on error.
BTPOV_Buffer * BTPOV_CreateBuffer(BTPOV_Instance * instance, VkDeviceSize size, BTPOV_BufferMode mode);

// Destroys a buffer. If the buffer is mapped, it is automatically unmapped.
// Returns null on error.
void BTPOV_DestroyBuffer(BTPOV_Buffer * buffer);

// Maps a buffer. A buffer may only be mapped at one place at a given time.
// A buffer of mode BTPOV_BUFFERMODE_GPU_ONLY may never be mapped.
// Attempting to double-map a buffer will result in returning the same map location.
// Returns null on error.
void * BTPOV_MapBuffer(BTPOV_Buffer * buffer);

// Unmaps a buffer. Note that this does not keep track of the amount of calls to MapBuffer, this *will* unmap the buffer.
void BTPOV_UnmapBuffer(BTPOV_Buffer * buffer);

// Flushes CPU reads from a buffer (for after GPU writes have been performed)
void BTPOV_FlushBufferRead(BTPOV_Buffer * buffer);

// Flushes CPU writes to a buffer (for after CPU writes have been performed)
void BTPOV_FlushBufferWrite(BTPOV_Buffer * buffer);

// --- Programs ---

typedef struct BTPOV_Program BTPOV_Program;

typedef struct BTPOV_ProgramDetails {
	// The amount of buffers used.
	size_t bufferCount;
	// The name of the procedure within the shader module to use.
	const char * name;
} BTPOV_ProgramDetails;

// Creates a program from a SPIR-V module containing a compute shader.
// The compute shader must have one descriptor set (0), and some number of storage buffers (one binding per storage buffer, starting at 0).
BTPOV_Program * BTPOV_CreateProgram(BTPOV_Instance * instance, const uint32_t * program, size_t programSize, const BTPOV_ProgramDetails * programDetails);

// A more "automatic" version of BTPOV_CreateProgram where fopen/etc. are available.
BTPOV_Program * BTPOV_CreateProgramFromFile(BTPOV_Instance * instance, const char * file, const BTPOV_ProgramDetails * programDetails);

typedef struct BTPOV_ProgramDetails2 {
	// The amount of buffers used.
	size_t bufferCount;
	// The name of the procedure within the shader module to use.
	const char * name;
	// Note that VkSpecializationInfo is used as-is, and therefore acts as in Vulkan, as there's no real reason not to.
	// This implies that specializationInfo may be NULL (but with these calls that would be pointless),
	//  and that specializationInfo need not outlive the BTPOV_Program.
	const VkSpecializationInfo * specializationInfo;
} BTPOV_ProgramDetails2;

// Like BTPOV_CreateProgram but takes a VkSpecializationInfo too.
BTPOV_Program * BTPOV_CreateProgram2(BTPOV_Instance * instance, const uint32_t * program, size_t programSize, const BTPOV_ProgramDetails2 * programDetails);

// Like BTPOV_CreateProgramFromFile but takes a VkSpecializationInfo too.
BTPOV_Program * BTPOV_CreateProgramFromFile2(BTPOV_Instance * instance, const char * file, const BTPOV_ProgramDetails2 * programDetails);

// Destroys a program.
void BTPOV_DestroyProgram(BTPOV_Program * program);

// --- Shader Modules ---

typedef struct BTPOV_ShaderModule BTPOV_ShaderModule;

// With the BTPOV_CreateProgram and BTPOV_CreateProgramFromFile APIs,
//  shader modules are managed by their parent Program (as no specialization applies).
// The split between Shader Modules and Programs is specifically to allow specialization to take place.
// This also conveniently keeps BTPOV_ProgramDetails unhindered by the change (see guideline on ABI preservation).

// Creates a shader module from a SPIR-V module containing a compute shader.
// The compute shader must have one descriptor set (0), and some number of storage buffers (one binding per storage buffer, starting at 0).
BTPOV_ShaderModule * BTPOV_CreateShaderModule(BTPOV_Instance * instance, const uint32_t * program, size_t programSize, size_t bufferCount);
// A more "automatic" version of BTPOV_CreateShaderModule where fopen/etc. are available.
BTPOV_ShaderModule * BTPOV_CreateShaderModuleFromFile(BTPOV_Instance * instance, const char * file, size_t bufferCount);

// Destroys a shader module.
void BTPOV_DestroyShaderModule(BTPOV_ShaderModule * shaderModule);

// Specializes a BTPOV_ShaderModule * into a BTPOV_Program *. This is the only reason to use BTPOV_ShaderModule.
// Note that VkSpecializationInfo is used as-is, and therefore acts as in Vulkan, as there's no real reason not to.
// This implies that specializationInfo may be NULL, and that specializationInfo need not outlive the BTPOV_ShaderModule.
BTPOV_Program * BTPOV_CreateProgramFromShaderModule(BTPOV_ShaderModule * shader, const char * name, const VkSpecializationInfo * specializationInfo);

// --- Buffered-Mode Execution API ---

typedef struct BTPOV_CommandBuffer BTPOV_CommandBuffer;

// Begins recording a command buffer. In this case, all commands to this instance return VK_SUCCESS but are not performed.
void BTPOV_BeginCommandBuffer(BTPOV_Instance * instance);

// Finishes recording a command buffer. If any error occurred during construction of the buffer, this returns NULL.
BTPOV_CommandBuffer * BTPOV_EndCommandBuffer(BTPOV_Instance * instance);

// Executes a command buffer, waiting until it completes.
// Note that the execution of a command buffer is not recorded in a command buffer.
VkResult BTPOV_ExecuteCommandBuffer(BTPOV_CommandBuffer * cmdbuf);

// Destroys a command buffer.
void BTPOV_DestroyCommandBuffer(BTPOV_CommandBuffer * cmdbuf);

// --- Immediate-Mode / Command Recording Execution API ---

// Regarding these:
// If a command buffer is not being recorded, the operation is performed immediately and waited upon in a scratch command buffer.
// If a command buffer is being recorded, then the operation is not actually performed, VK_SUCCESS is returned, and the operation is recorded into the command buffer.
// Otherwise, they return non-VK_SUCCESS for error.

// Executes a program with some set of buffers (the amount is as passed when creating the program) and some amount of workgroups, waiting until this completes.
VkResult BTPOV_ExecuteProgram(BTPOV_Program * program, BTPOV_Buffer ** buffers, uint32_t x, uint32_t y, uint32_t z);

// Copies data between buffers. A reasonably direct wrapper around vkCmdCopyBuffer.
VkResult BTPOV_ExecuteBufferCopy(BTPOV_Buffer * src, BTPOV_Buffer * dst, uint32_t regionCount, const VkBufferCopy * pRegions);

// Writes to a buffer in a command list. A reasonably direct wrapper around vkCmdUpdateBuffer.
// Do consider other methods instead, but this is certainly more convenient.
VkResult BTPOV_ExecuteBufferUpdate(BTPOV_Buffer * dst, VkDeviceSize ofs, VkDeviceSize size, const void * data);

// --- Instances / Debug Introspection ---

// Copies the VkPhysicalDeviceProperties of the instance to the target.
// This may return unreliable values if the information is not available and is for introspection only.
// In particular, see BTPOV_CreateInstanceVulkanExplicit - if the passed physical device properties were incorrect, then the resulting physical device properties will thus also be incorrect.
void BTPOV_GetInstancePhysicalDeviceProperties(BTPOV_Instance * instance, VkPhysicalDeviceProperties * details);

// Returns the bitfield of valid memory types for a given buffer mode, assuming vkGetBufferMemoryRequirements returns all 1s.
// If optimal is non-zero, memory types won't be considered valid if they are not optimal.
// This does not discriminate based on heap index.
// This may return unreliable values if the information is not available and is for introspection only.
uint32_t BTPOV_GetMemoryTypesForBufferMode(BTPOV_Instance * instance, BTPOV_BufferMode mode, int optimal);

#endif
