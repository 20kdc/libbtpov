# Notes in regards to interaction between libbtpov and AMD APUs

## Current Situation

As always, `BTPOV_CREATEINSTANCE_FLAGS_DONT_HSA_CHECK` should not be turned on without either user consent or if the application has manually chosen the Vulkan device.

Observing Mesa history, it appears Mesa commit 7a0a6a718035e1a754972fbbad8b91d19f39fa42 `radv: do not expose GTT as device local memory mostly for APUs` has prevented any critical need for the 'HSA check' *on RADV drivers.*

However, other drivers may attempt to imitate RADV's example, or users may be using outdated RADV drivers.

Therefore the check and the corresponding flag remains in place.

## Primer

Vulkan-capable GPU designs do not require that data be explicitly shifted to the GPU before use.

The GPUs are implicitly always capable of streaming data directly from system RAM (see `Vulkan 1.0.153 - A Specification` PDF, Chapter 10, Memory Allocation, page 257)

(I assume this capability is expected to be emulated if necessary - this is one of the few times Vulkan actually operates in favour of application developer ease of use, soured slightly by the rest of Vulkan memory management.)

Integrated GPUs, where not NVIDIA-style, do not have dedicated RAM.

However, AMD APUs have "reserved RAM" carved out by the BIOS to ensure it is reserved solely for APU use.

As far as I can reasonably tell, there's no hardware difference between it and regular RAM, and some BIOSes allow configuring the amount.

Some BIOSes, however, *don't* allow configuring the amount, and what's even worse is, the amount can be 2GB in a system with 8GB of total RAM and a 2c4t CPU.

So in this situation, the system has bled off 2GB of VRAM when it's not going to reasonably be able to run a program that can *use* 2GB of VRAM.

In this situation, it is *crucial* that whenever a program can find some excuse to use the VRAM, it does, since nothing else (Firefox, Chromium, and the many applications which embed Chromium for some reason or another are good examples of RAM-gobblers) is going to.

## (Old) RADV behaviour & the "HSA check"

*This covers previous versions of RADV. Mainline RADV does not have this issue.*

At the original time of the creation of the (badly named) "HSA check" during development of libbtpov, RADV's behaviour on an APU was to expose both heap and GTT RAM as "device-local RAM", or VRAM.

The problem with this approach is that there is no way for code to sanely distingulish between the VRAM and the GTT RAM.

Luckily, the GTT RAM was 3GB and the VRAM was 2GB.

For libbtpov's purposes, this lead to a simple heuristic:

If all memory is device-local, the largest heap is GTT, avoid it.

That was the HSA check.

## New RADV behavior

As of Mesa commit 7a0a6a718035e1a754972fbbad8b91d19f39fa42 `radv: do not expose GTT as device local memory mostly for APUs`, the 'HSA check' has become useless *on RADV drivers.*

The problems with simply removing the 'HSA check' are twofold:

+ Older drivers
+ Other drivers that try the same thing

## Future notes

A smaller problem has arose that may require an extension of the check, that being that libbtpov will (of course) start using CPU RAM when buffer modes permit even though this is the 'wrong thing' (as it's using memory that could be placed in dedicated VRAM with no penalty).

However doing that offers more potential for severe 'BTPOV did the wrong thing' trouble, while the current behavior will always do something reasonable even if not perfect.

If presented with a "all device-local" situation that would normally guarantee incorrect use of system RAM, it'll trigger the HSA check and avoid system RAM if possible.

Assuming that VRAM will always be smaller than exposed system RAM, this will be fine.

Under the situation present RADV provides, it will act like a program that is not aware it is running on an APU, and CPU buffers will be placed in system RAM.

The problem with HSA-checking this situation is that it could effectively nullify `BTPOV_BUFFERMODE_CPU` if a false positive occurs.

The good news is that `BTPOV_BUFFERMODE_CPU` is effectively a hint, and does not change semantics under the current BTPOV API.

The bad news is it'd still be bad to nullify it, as it is implicitly assumed the application had a reason to put the buffer in system memory rather than GPU memory.
Ignoring this is beneficial on APUs but may be detrimental if it occurs on something which is not an APU.

