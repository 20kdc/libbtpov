HEADERS := include/libbtpov.h src/btpov_internal.h
OBJECTS := obj/instance.o obj/report.o obj/buffer.o obj/shadermodule.o obj/program.o obj/immediate.o obj/list.o obj/cmdbuf.o

all: libbtpov.a _examples_c _examples_spv

_examples_c: examples/simple examples/simplespecialized
_examples_c: examples/which examples/cmdbuf examples/copyupdate
_examples_c: examples/findcrc
_examples_c: examples/pathtracer

_examples_spv: examples/simple.spv examples/simplespecialized.spv
_examples_spv: examples/cmdbuf.spv
_examples_spv: examples/findcrc.poly.spv examples/findcrc.core.spv
_examples_spv: examples/pathtracer.spv

clean:
	rm -f libbtpov.a $(OBJECTS)

examples/%: examples/%.c $(HEADERS) libbtpov.a
	cc -o $@ -Iinclude $< libbtpov.a -lvulkan

examples/%.spv: examples/%.comp.glsl
	glslangValidator -V $< -o $@

libbtpov.a: $(OBJECTS)
	rm -f libbtpov.a
	ar q libbtpov.a $(OBJECTS)

obj/%.o: src/%.c $(HEADERS)
	cc -c -o $@ $<

