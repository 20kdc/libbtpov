#ifndef BTPOV_INTERNAL_h
#define BTPOV_INTERNAL_h
#include "../include/libbtpov.h"
#include <stdlib.h>
#include <stdio.h>

// -- list --

typedef struct BTPOV_List {
	void * content;
	size_t size;
} BTPOV_List;

// Returns non-zero on error, and the list is not changed.
int BTPOV_ListAdd(BTPOV_List * list, void * stuff, size_t amount);

// -- reporter --

void BTPOV_ReportError(const char * err);

// -- private sides of public objects --

typedef struct BTPOV_RecordState {
	int api; // Non-zero if we're supposed to be recording.
	VkCommandBuffer cmdBuf; // If VK_NULL_HANDLE, there is no active being-recorded-to command buffer.
	BTPOV_List descriptorSetList; // If empty, there is no being-recorded-to buffer
} BTPOV_RecordState;

struct BTPOV_Instance {
	// -- core objects --
	VkInstance instance; // If VK_NULL_HANDLE, BTPOV does not own this
	int deviceQueuePoolOwned;
	VkDevice device;
	VkQueue queue;
	VkCommandPool commandPool;
	// --
	BTPOV_RecordState recording;
	BTPOV_Reporter * reporter;
	int hsaAvoidThisHeap; // -1: deactivated
	VkPhysicalDeviceProperties physicalDeviceProperties;
	VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
};

struct BTPOV_Buffer {
	BTPOV_Instance * instance;
	VkBuffer buffer;
	VkDeviceMemory memory;
	BTPOV_BufferMode mode;
	void * map;
};

struct BTPOV_Program {
	int ownsShaderModule; // controls if bModule ought to be auto-freed
	BTPOV_ShaderModule * bModule;
	VkPipeline pipeline;
};

struct BTPOV_ShaderModule {
	BTPOV_Instance * instance;
	VkShaderModule shaderModule;
	VkPipelineLayout pipelineLayout;
	VkDescriptorSetLayout descriptorSetLayout;
	size_t bufferCount;
};

struct BTPOV_CommandBuffer {
	BTPOV_Instance * instance;
	VkCommandBuffer cmdBuf;
	// Cleanup lists (in order of deletion)
	BTPOV_List descriptorSetList;
};

// -- command management --

typedef struct {
	BTPOV_Instance * instance;
	VkCommandBuffer cmdBuf;
	VkResult error; // The error during opening a command.
	// -- things set by the command --
	VkDescriptorPool descriptorPool;
} BTPOV_OpenedCommand;

// May return zero, in which case do not write any actual commands be written.
// However, you still need to close the command to cause it to properly close resources that you would otherwise leak.
// Otherwise, returns one.
int BTPOV_OpenCommand(BTPOV_Instance * instance, BTPOV_OpenedCommand * res);
VkResult BTPOV_CloseCommand(BTPOV_OpenedCommand * res);

VkResult BTPOV_TransferErrorToRecording(BTPOV_Instance * instance, VkResult error);
void BTPOV_ClearDescriptorPoolSetPairList(BTPOV_Instance * instance, BTPOV_List * target);

void BTPOV_TruePipelineBarrier(VkCommandBuffer cmdBuf);

#endif
