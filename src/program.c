#include "btpov_internal.h"

BTPOV_Program * BTPOV_CreateProgramFromShaderModule(BTPOV_ShaderModule * shader, const char * name, const VkSpecializationInfo * specializationInfo) {
	VkPipelineShaderStageCreateInfo pssci = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = VK_SHADER_STAGE_COMPUTE_BIT,
		.module = shader->shaderModule,
		.pName = name,
		.pSpecializationInfo = specializationInfo
	};
	VkComputePipelineCreateInfo pci = {
		.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
		.stage = pssci,
		.layout = shader->pipelineLayout
	};
	VkPipeline vmPipeline = VK_NULL_HANDLE;
	if (vkCreateComputePipelines(shader->instance->device, VK_NULL_HANDLE, 1, &pci, NULL, &vmPipeline) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateProgram: Failed to create compute pipeline.");
		return NULL;
	}
	BTPOV_Program * program = malloc(sizeof(BTPOV_Program));
	if (program) {
		program->ownsShaderModule = 0;
		program->bModule = shader;
		program->pipeline = vmPipeline;
	} else {
		BTPOV_ReportError("BTPOV_CreateProgram: Could not malloc API structure.");
		vkDestroyPipeline(shader->instance->device, vmPipeline, NULL);
		program = NULL;
	}
	return program;
}

void BTPOV_DestroyProgram(BTPOV_Program * program) {
	vkDestroyPipeline(program->bModule->instance->device, program->pipeline, NULL);
	if (program->ownsShaderModule)
		BTPOV_DestroyShaderModule(program->bModule);
	free(program);
}

// Wrappers over shader module creation functions - v1

static void BTPOV_UpdateProgramDetails1(BTPOV_ProgramDetails2 * programDetailsNx, const BTPOV_ProgramDetails * programDetails) {
	programDetailsNx->bufferCount = programDetails->bufferCount;
	programDetailsNx->name = programDetails->name;
	programDetailsNx->specializationInfo = NULL;
}

BTPOV_Program * BTPOV_CreateProgram(BTPOV_Instance * instance, const uint32_t * programCode, size_t programSize, const BTPOV_ProgramDetails * programDetails) {
	BTPOV_ProgramDetails2 pds;
	BTPOV_UpdateProgramDetails1(&pds, programDetails);
	return BTPOV_CreateProgram2(instance, programCode, programSize, &pds);
}

BTPOV_Program * BTPOV_CreateProgramFromFile(BTPOV_Instance * instance, const char * file, const BTPOV_ProgramDetails * programDetails) {
	BTPOV_ProgramDetails2 pds;
	BTPOV_UpdateProgramDetails1(&pds, programDetails);
	return BTPOV_CreateProgramFromFile2(instance, file, &pds);
}

// Wrappers over shader module creation functions - v2

static BTPOV_Program * BTPOV_CreateProgramFromShaderModule_Owned(BTPOV_ShaderModule * sm, const BTPOV_ProgramDetails2 * sp) {
	if (!sm)
		return NULL;
	BTPOV_Program * p = BTPOV_CreateProgramFromShaderModule(sm, sp->name, sp->specializationInfo);
	if (!p) {
		BTPOV_DestroyShaderModule(sm);
	} else {
		p->ownsShaderModule = 1;
	}
	return p;
}

BTPOV_Program * BTPOV_CreateProgram2(BTPOV_Instance * instance, const uint32_t * programCode, size_t programSize, const BTPOV_ProgramDetails2 * programDetails) {
	return BTPOV_CreateProgramFromShaderModule_Owned(BTPOV_CreateShaderModule(instance, programCode, programSize, programDetails->bufferCount), programDetails);
}

BTPOV_Program * BTPOV_CreateProgramFromFile2(BTPOV_Instance * instance, const char * file, const BTPOV_ProgramDetails2 * programDetails) {
	return BTPOV_CreateProgramFromShaderModule_Owned(BTPOV_CreateShaderModuleFromFile(instance, file, programDetails->bufferCount), programDetails);
}

