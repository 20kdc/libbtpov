#include "btpov_internal.h"

BTPOV_ShaderModule * BTPOV_CreateShaderModule(BTPOV_Instance * instance, const uint32_t * programCode, size_t programSize, size_t bufferCount) {
	VkShaderModule vmShaderModule = VK_NULL_HANDLE;
	VkShaderModuleCreateInfo ci = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.pCode = (const uint32_t *) programCode,
		.codeSize = programSize
	};
	if (vkCreateShaderModule(instance->device, &ci, NULL, &vmShaderModule) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateShaderModule: Failed to create shader module.");
		return NULL;
	}
	VkDescriptorSetLayoutBinding * bindings = malloc(sizeof(VkDescriptorSetLayoutBinding) * bufferCount);
	for (size_t i = 0; i < bufferCount; i++) {
		VkDescriptorSetLayoutBinding dslce = {
			.binding = i,
			.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT
		};
		bindings[i] = dslce;
	}
	VkDescriptorSetLayoutCreateInfo dslci = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.bindingCount = bufferCount,
		.pBindings = bindings
	};
	VkDescriptorSetLayout vmDescriptorSetLayout = VK_NULL_HANDLE;
	if (vkCreateDescriptorSetLayout(instance->device, &dslci, NULL, &vmDescriptorSetLayout) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateShaderModule: Failed to create descriptor set layout.");
		free(bindings);
		goto hasShaderModuleAndError;
	}
	free(bindings);

	VkPipelineLayoutCreateInfo plci = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.setLayoutCount = 1,
		.pSetLayouts = &vmDescriptorSetLayout
	};
	VkPipelineLayout vmPipelineLayout = VK_NULL_HANDLE;
	if (vkCreatePipelineLayout(instance->device, &plci, NULL, &vmPipelineLayout) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateShaderModule: Failed to create pipeline layout.");
		goto hasDescriptorSetLayoutAndError;
	}
	BTPOV_ShaderModule * sm = malloc(sizeof(BTPOV_ShaderModule));
	if (sm) {
		sm->instance = instance;
		sm->shaderModule = vmShaderModule;
		sm->pipelineLayout = vmPipelineLayout;
		sm->descriptorSetLayout = vmDescriptorSetLayout;
		sm->bufferCount = bufferCount;
	} else {
		BTPOV_ReportError("BTPOV_CreateShaderModule: Could not malloc API structure.");
		vkDestroyPipelineLayout(instance->device, vmPipelineLayout, NULL);
		hasDescriptorSetLayoutAndError:
		vkDestroyDescriptorSetLayout(instance->device, vmDescriptorSetLayout, NULL);
		hasShaderModuleAndError:
		vkDestroyShaderModule(instance->device, vmShaderModule, NULL);
		sm = NULL;
	}
	return sm;
}

BTPOV_ShaderModule * BTPOV_CreateShaderModuleFromFile(BTPOV_Instance * instance, const char * file, size_t bufferCount) {
	FILE * f = fopen(file, "rb");
	if (!f) {
		BTPOV_ReportError("BTPOV_CreateShaderModuleFromFile: Unable to open file");
		return NULL;
	}
	fseek(f, 0, SEEK_END);
	long pos = ftell(f);
	fseek(f, 0, SEEK_SET);
	char * data = malloc(pos);
	if (!data) {
		fclose(f);
		BTPOV_ReportError("BTPOV_CreateShaderModuleFromFile: Unable to malloc data buffer");
		return NULL;
	}
	fread(data, pos, 1, f);
	fclose(f);
	BTPOV_ShaderModule * sm = BTPOV_CreateShaderModule(instance, (const uint32_t *) data, pos, bufferCount);
	free(data);
	return sm;
}

void BTPOV_DestroyShaderModule(BTPOV_ShaderModule * sm) {
	vkDestroyPipelineLayout(sm->instance->device, sm->pipelineLayout, NULL);
	vkDestroyDescriptorSetLayout(sm->instance->device, sm->descriptorSetLayout, NULL);
	vkDestroyShaderModule(sm->instance->device, sm->shaderModule, NULL);
	free(sm);
}

