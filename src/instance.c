#include "btpov_internal.h"

BTPOV_Instance * BTPOV_CreateInstanceAutomatic(uint32_t flags, BTPOV_Reporter * reporter) {
	VkInstanceCreateInfo createInfo = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO
	};
	VkInstance vmInstance = VK_NULL_HANDLE;
	if (vkCreateInstance(&createInfo, NULL, &vmInstance) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateInstanceAutomatic: Unable to create Vulkan instance.");
		return NULL;
	}

	uint32_t dump = 16;
	VkPhysicalDevice vmPhysicalDevices[16];
	vkEnumeratePhysicalDevices(vmInstance, &dump, vmPhysicalDevices);
	if (!dump) {
		BTPOV_ReportError("BTPOV_CreateInstanceAutomatic: Unable to find any Vulkan devices.");
		goto hasInstanceAndError;
	}

	while (1) {
		uint32_t devIdx = 0;
		int vmPhysicalDevicePriority = -1;
		for (uint32_t i = 0; i < dump; i++) {
			if (vmPhysicalDevices[i] == VK_NULL_HANDLE)
				continue;
			VkPhysicalDeviceProperties props;
			vkGetPhysicalDeviceProperties(vmPhysicalDevices[i], &props);
			int priority = 0;
			if (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU)
				priority = 1;
			if (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
				priority = 2;
			if (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
				priority = 3;
			if (priority > vmPhysicalDevicePriority) {
				devIdx = i;
				vmPhysicalDevicePriority = priority;
			}
		}
		if (vmPhysicalDevices[devIdx] == VK_NULL_HANDLE) {
			BTPOV_ReportError("BTPOV_CreateInstanceAutomatic: Was unable to successfully initialize with any Vulkan device.");
			goto hasInstanceAndError;
		}

		BTPOV_Instance * instance = BTPOV_CreateInstanceVulkan(vmInstance, vmPhysicalDevices[devIdx], flags, reporter);
		if (instance != NULL) {
			// Mark the VkInstance so that it will be destroyed automatically, then return the whole thing
			instance->instance = vmInstance;
			return instance;
		} else {
			// Device failed! Mark the device as null to prevent an infinite loop, then try to find another device.
			vmPhysicalDevices[devIdx] = VK_NULL_HANDLE;
		}
	}

	hasInstanceAndError:
	vkDestroyInstance(vmInstance, NULL);
	return NULL;
}

BTPOV_Instance * BTPOV_CreateInstanceVulkan(VkInstance instance, VkPhysicalDevice vmPhysicalDevice, uint32_t flags, BTPOV_Reporter * reporter) {
	uint32_t queueFamilyCount = 16;
	VkQueueFamilyProperties qfProps[16];
	vkGetPhysicalDeviceQueueFamilyProperties(vmPhysicalDevice, &queueFamilyCount, qfProps);

	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(vmPhysicalDevice, &memoryProperties);

	uint32_t queueFamilyIndex;
	// Please see the note in Vulkan spec version 1.0.165 at the end of 5.1 Physical Devices (at page 51 in the PDF)
	//  that more or less states that VK_QUEUE_COMPUTE_BIT or VK_QUEUE_GRAPHICS_BIT both imply VK_QUEUE_TRANSFER_BIT.
	VkQueueFlagBits requiredQueueFlagBits = VK_QUEUE_COMPUTE_BIT;
	for (queueFamilyIndex = 0; queueFamilyIndex < queueFamilyCount; queueFamilyIndex++)
		if ((qfProps[queueFamilyIndex].queueFlags & requiredQueueFlagBits) == requiredQueueFlagBits)
			break;
	if (queueFamilyIndex == queueFamilyCount) {
		BTPOV_ReportError("BTPOV_CreateInstanceVulkan: Unable to find a suitable queue family.");
		return NULL;
	}

	float ignoreMePlease = 1.0f;
	VkDeviceQueueCreateInfo mainQueueCI = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.queueFamilyIndex = queueFamilyIndex,
		.queueCount = 1,
		.pQueuePriorities = &ignoreMePlease
	};
	VkDeviceCreateInfo dCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &mainQueueCI
	};
	VkDevice vmDevice = VK_NULL_HANDLE;
	if (vkCreateDevice(vmPhysicalDevice, &dCreateInfo, NULL, &vmDevice) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateInstanceVulkan: Unable to create Vulkan device.");
		return NULL;
	}
	VkQueue vmQueue = VK_NULL_HANDLE;
	vkGetDeviceQueue(vmDevice, 0, 0, &vmQueue);
	VkCommandPoolCreateInfo cpci = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.queueFamilyIndex = queueFamilyIndex
	};
	VkCommandPool vmCommandPool = VK_NULL_HANDLE;
	if (vkCreateCommandPool(vmDevice, &cpci, NULL, &vmCommandPool) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateInstanceVulkan: Unable to create Vulkan command pool.");
		goto hasDeviceAndError;
	}

	VkPhysicalDeviceProperties details;
	vkGetPhysicalDeviceProperties(vmPhysicalDevice, &details);
	BTPOV_Instance * res = BTPOV_CreateInstanceVulkanExplicit(vmDevice, vmQueue, vmCommandPool, &details, &memoryProperties, flags, reporter);
	if (res) {
		// we do actually own these (but not the Instance, as far as this function is concerned)
		res->deviceQueuePoolOwned = 1;
	} else {
		vkDestroyCommandPool(vmDevice, vmCommandPool, NULL);
		hasDeviceAndError:
		vkDestroyDevice(vmDevice, NULL);
		res = NULL;
	}
	return res;
}

BTPOV_Instance * BTPOV_CreateInstanceVulkanExplicit(VkDevice device, VkQueue queue, VkCommandPool pool, const VkPhysicalDeviceProperties * info, const VkPhysicalDeviceMemoryProperties * memoryInfo, uint32_t flags, BTPOV_Reporter * reporter) {
	BTPOV_Instance * instance = malloc(sizeof(BTPOV_Instance));
	if (!instance) {
		BTPOV_ReportError("BTPOV_CreateInstanceVulkanExplicit: Could not malloc API structure.");
		return NULL;
	}
	// --
	instance->instance = VK_NULL_HANDLE;
	instance->deviceQueuePoolOwned = 0;
	instance->device = device;
	instance->queue = queue;
	instance->commandPool = pool;
	// --
	BTPOV_RecordState emptyRecordState = {};
	instance->recording = emptyRecordState;
	instance->reporter = reporter;
	instance->hsaAvoidThisHeap = -1;
	instance->physicalDeviceProperties = *info;
	instance->physicalDeviceMemoryProperties = *memoryInfo;
	// Attempt to detect HSA.
	if (!(flags & BTPOV_CREATEINSTANCE_FLAGS_DONT_HSA_CHECK)) {
		int isHSA = 1;
		for (uint32_t i = 0; i < memoryInfo->memoryHeapCount; i++) {
			if (!(memoryInfo->memoryHeaps[i].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)) {
				isHSA = 0;
				break;
			}
		}
		// If we got here and isHSA is still 1, we didn't find any heaps which weren't device-local.
		// This means we're on an HSA system, and the largest heap is going to be system RAM.
		// Which we would prefer not to use for memory management reasons.
		if (isHSA) {
			VkDeviceSize heapSize = 0;
			for (int i = 0; i < memoryInfo->memoryHeapCount; i++) {
				VkDeviceSize sz = memoryInfo->memoryHeaps[i].size;
				if (sz > heapSize) {
					instance->hsaAvoidThisHeap = i;
					heapSize = sz;
				}
			}
		}
	}
	return instance;
}

void BTPOV_DestroyInstance(BTPOV_Instance * instance) {
	if (instance->deviceQueuePoolOwned) {
		// delete this lot first
		vkDestroyCommandPool(instance->device, instance->commandPool, NULL);
		vkDestroyDevice(instance->device, NULL);
	}
	if (instance->instance != VK_NULL_HANDLE)
		vkDestroyInstance(instance->instance, NULL);
	free(instance);
}

// Copies the VkPhysicalDeviceProperties of the instance to the target.
void BTPOV_GetInstancePhysicalDeviceProperties(BTPOV_Instance * instance, VkPhysicalDeviceProperties * details) {
	*details = instance->physicalDeviceProperties;
}

