#include "btpov_internal.h"

void BTPOV_TruePipelineBarrier(VkCommandBuffer cmdBuf) {
	VkMemoryBarrier mb = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
		.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT,
		.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT
	};
	vkCmdPipelineBarrier(cmdBuf, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1, &mb, 0, NULL, 0, NULL);
}

static void BTPOV_DestroyRecordState(BTPOV_Instance * instance) {
	if (instance->recording.cmdBuf != VK_NULL_HANDLE) {
		vkFreeCommandBuffers(instance->device, instance->commandPool, 1, &instance->recording.cmdBuf);
		instance->recording.cmdBuf = VK_NULL_HANDLE;
	}
	BTPOV_ClearDescriptorPoolSetPairList(instance, &instance->recording.descriptorSetList);
}

void BTPOV_BeginCommandBuffer(BTPOV_Instance * instance) {
	if (instance->recording.api) {
		BTPOV_ReportError("BTPOV_BeginCommandBuffer: Already recording a command buffer.");
		return;
	}
	instance->recording.api = 1;

	VkCommandBufferAllocateInfo cai = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandBufferCount = 1,
		.commandPool = instance->commandPool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
	};
	VkResult error = vkAllocateCommandBuffers(instance->device, &cai, &instance->recording.cmdBuf);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_BeginCommandBuffer: Failed to allocate command buffer.");
		instance->recording.cmdBuf = VK_NULL_HANDLE;
		return;
	}
	VkCommandBufferBeginInfo bi = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT
	};
	error = vkBeginCommandBuffer(instance->recording.cmdBuf, &bi);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_BeginCommandBuffer: Failed to begin command buffer.");
		BTPOV_DestroyRecordState(instance);
		return;
	}
}

BTPOV_CommandBuffer * BTPOV_EndCommandBuffer(BTPOV_Instance * instance) {
	if (!instance->recording.api) {
		BTPOV_ReportError("BTPOV_EndCommandBuffer: Not recording a command buffer.");
		return NULL;
	}
	instance->recording.api = 0;

	// Is it already dead?
	if (instance->recording.cmdBuf == VK_NULL_HANDLE)
		return NULL;

	// Epilogue pipeline barrier
	BTPOV_TruePipelineBarrier(instance->recording.cmdBuf);

	if (vkEndCommandBuffer(instance->recording.cmdBuf) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_EndCommandBuffer: Failed to end command buffer.");
		BTPOV_DestroyRecordState(instance);
		return NULL;
	}

	BTPOV_CommandBuffer * cmdBuf = malloc(sizeof(BTPOV_CommandBuffer));
	if (!cmdBuf) {
		BTPOV_ReportError("BTPOV_EndCommandBuffer: Failed to allocate structure.");
		BTPOV_DestroyRecordState(instance);
		return NULL;
	}

	cmdBuf->instance = instance;
	cmdBuf->cmdBuf = instance->recording.cmdBuf;
	cmdBuf->descriptorSetList = instance->recording.descriptorSetList;

	BTPOV_RecordState emptyRecordState = {};
	instance->recording = emptyRecordState;

	return cmdBuf;
}

static VkResult BTPOV_ExecuteCommandBufferInternal(BTPOV_Instance * instance, VkCommandBuffer vmCommandBuffer) {
	VkSubmitInfo si = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.commandBufferCount = 1,
		.pCommandBuffers = &vmCommandBuffer,
	};
	VkResult error = vkQueueSubmit(instance->queue, 1, &si, VK_NULL_HANDLE);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_ExecuteCommandBufferInternal: Could not submit.");
		return error;
	}

	error = vkQueueWaitIdle(instance->queue);
	if (error != VK_SUCCESS)
		BTPOV_ReportError("BTPOV_ExecuteCommandBufferInternal: Could not wait until idle.");
	return error;
}

VkResult BTPOV_ExecuteCommandBuffer(BTPOV_CommandBuffer * cmdbuf) {
	return BTPOV_ExecuteCommandBufferInternal(cmdbuf->instance, cmdbuf->cmdBuf);
}

void BTPOV_DestroyCommandBuffer(BTPOV_CommandBuffer * cmdbuf) {
	vkFreeCommandBuffers(cmdbuf->instance->device, cmdbuf->instance->commandPool, 1, &cmdbuf->cmdBuf);
	BTPOV_ClearDescriptorPoolSetPairList(cmdbuf->instance, &cmdbuf->descriptorSetList);
	free(cmdbuf);
}

// --

int BTPOV_OpenCommand(BTPOV_Instance * instance, BTPOV_OpenedCommand * res) {
	res->instance = instance;
	res->error = VK_SUCCESS;
	if (instance->recording.api) {
		res->cmdBuf = instance->recording.cmdBuf;
		if (res->cmdBuf == VK_NULL_HANDLE)
			return 0;
		// Prologue pipeline barrier
		BTPOV_TruePipelineBarrier(res->cmdBuf);
		return 1;
	}

	VkCommandBufferAllocateInfo cai = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandBufferCount = 1,
		.commandPool = instance->commandPool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
	};
	VkResult error = vkAllocateCommandBuffers(instance->device, &cai, &res->cmdBuf);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_OpenCommand: Failed to allocate command buffer.");
		res->cmdBuf = VK_NULL_HANDLE;
		res->error = error;
		return 0;
	}
	VkCommandBufferBeginInfo bi = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT
	};
	error = vkBeginCommandBuffer(res->cmdBuf, &bi);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_OpenCommand: Failed to begin command buffer.");
		res->error = error;
		vkFreeCommandBuffers(instance->device, instance->commandPool, 1, &res->cmdBuf);
		res->cmdBuf = VK_NULL_HANDLE;
		return 0;
	}
	// Prologue pipeline barrier
	BTPOV_TruePipelineBarrier(res->cmdBuf);
	return 1;
}

VkResult BTPOV_CloseCommand(BTPOV_OpenedCommand * res) {
	VkResult error = VK_SUCCESS;
	if (res->instance->recording.api) {
		// Recording
		if (res->instance->recording.cmdBuf != VK_NULL_HANDLE) {
			// Add descriptorPool.
			if (res->descriptorPool != VK_NULL_HANDLE) {
				if (BTPOV_ListAdd(&res->instance->recording.descriptorSetList, &res->descriptorPool, sizeof(VkDescriptorPool))) {
					// Failed, destroy record state and fall back to no-command-buffer
					BTPOV_ReportError("BTPOV_CloseCommand: Failed to add descriptor pool used by command to cleanup list.");
					BTPOV_DestroyRecordState(res->instance);
					goto failedToRecord;
				}
			}
			// Success, don't clean up descriptors as their ownership has been transferred
			return error;
		}
		// Recording - No command buffer
		// do nothing, descriptors will be cleaned up
	} else {
		// Not recording
		if (res->cmdBuf != VK_NULL_HANDLE) {
			// Immediate command execute
			// Epilogue pipeline barrier
			BTPOV_TruePipelineBarrier(res->cmdBuf);
			// Actually do the thing
			error = vkEndCommandBuffer(res->cmdBuf);
			if (error != VK_SUCCESS) {
				BTPOV_ReportError("BTPOV_CloseCommand: Failed to end command buffer.");
				return error;
			}
			BTPOV_ExecuteCommandBufferInternal(res->instance, res->cmdBuf);
			vkFreeCommandBuffers(res->instance->device, res->instance->commandPool, 1, &res->cmdBuf);
		} else {
			// Not recording - No command buffer
			error = res->error;
		}
	}
	// If we get here, clean up descriptors
	failedToRecord:
	vkDestroyDescriptorPool(res->instance->device, res->descriptorPool, NULL);
	return error;
}

// --

VkResult BTPOV_TransferErrorToRecording(BTPOV_Instance * instance, VkResult error) {
	if (instance->recording.api) {
		BTPOV_DestroyRecordState(instance);
		return VK_SUCCESS;
	}
	return error;
}

void BTPOV_ClearDescriptorPoolSetPairList(BTPOV_Instance * instance, BTPOV_List * target) {
	size_t pos = 0;
	while (pos < target->size) {
		// fun fact: this used to free the set first, hence the function name
		// but that was actually a violation of spec
		// for reference:
		// VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_standard_validation ./cmdbuf
		vkDestroyDescriptorPool(instance->device, *((VkDescriptorPool *) (target->content + pos)), NULL);
		pos += sizeof(VkDescriptorPool);
	}
	free(target->content);
	target->content = NULL;
	target->size = 0;
}

