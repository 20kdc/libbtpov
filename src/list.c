#include "btpov_internal.h"
#include <string.h>

// Returns non-zero on error, and the list is not changed.
int BTPOV_ListAdd(BTPOV_List * list, void * stuff, size_t amount) {
	void * result = realloc(list->content, list->size + amount);
	if (!result)
		return 1;
	list->content = result;
	memcpy(list->content + list->size, stuff, amount);
	list->size += amount;
	return 0;
}

