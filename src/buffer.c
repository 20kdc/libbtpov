#include "btpov_internal.h"

static uint32_t BTPOV_GetMemoryTypesForHeap(VkPhysicalDeviceMemoryProperties * pdmp, int heap) {
	uint32_t res = 0;
	for (int i = 0; i < pdmp->memoryTypeCount; i++) {
		if (pdmp->memoryTypes[i].heapIndex == heap)
			res |= 1U << i;
	}
	return res;
}

// note: hasNot has VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT implied
static uint32_t BTPOV_GetMemoryTypesForFlagsPair(BTPOV_Instance * instance, VkMemoryPropertyFlags has, VkMemoryPropertyFlags hasNot) {
	hasNot |= VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
	uint32_t res = 0;
	for (int i = 0; i < instance->physicalDeviceMemoryProperties.memoryTypeCount; i++) {
		VkMemoryPropertyFlags propFlags = instance->physicalDeviceMemoryProperties.memoryTypes[i].propertyFlags;
		if ((propFlags & has) != has)
			continue;
		if (propFlags & hasNot)
			continue;
		res |= 1U << i;
	}
	return res;
}

uint32_t BTPOV_GetMemoryTypesForBufferMode(BTPOV_Instance * instance, BTPOV_BufferMode mode, int optimal) {
	if (mode == BTPOV_BUFFERMODE_CPU) {
		if (optimal) {
			// This is a bit complicated. Ideally, we wouldn't use device-local RAM for BTPOV_BUFFERMODE_CPU.
			// However, on APUs, everything is device local.
			// So in this case we want to fall back to 'slightly less optimal' memory without going to 'any memory'.
			uint32_t b = BTPOV_GetMemoryTypesForFlagsPair(instance, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
			// And when everything's device-local, nothing will be
			if (b == 0)
				b = BTPOV_GetMemoryTypesForFlagsPair(instance, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 0);
			return b;
		}
		// Sub-optimally, the host and device just have to be able to see it.
		return BTPOV_GetMemoryTypesForFlagsPair(instance, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, 0);
	} else if (mode == BTPOV_BUFFERMODE_GPU) {
		// Optimally, this would be device-local memory the host can see.
		if (optimal)
			return BTPOV_GetMemoryTypesForFlagsPair(instance, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 0);
		// Sub-optimally, the host and device just have to be able to see it.
		return BTPOV_GetMemoryTypesForFlagsPair(instance, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, 0);
	} else if (mode == BTPOV_BUFFERMODE_GPU_ONLY) {
		// Optimally, this would be device-local memory the host can't see (indicating to Vulkan that we will not ever be mapping it).
		if (optimal)
			return BTPOV_GetMemoryTypesForFlagsPair(instance, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		// Sub-optimally, we don't care about the memory type at all.
		return BTPOV_GetMemoryTypesForFlagsPair(instance, 0, 0);
	} else {
		BTPOV_ReportError("BTPOV_GetMemoryTypesForBufferMode: Invalid buffer mode");
	}
	return 0;
}

// This is the 'inner allocator', following the bare-bones "search over types" loop without general parameterization.
// Returns VK_NULL_HANDLE on failure.
static VkDeviceMemory BTPOV_AllocateDeviceMemoryOverTypeField(BTPOV_Instance * instance, VkDeviceSize size, uint32_t types) {
	if (types == 0)
		return VK_NULL_HANDLE;
	VkMemoryAllocateInfo pAllocate = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = size,
		.memoryTypeIndex = 0
	};
	// Attempt to allocate the memory (finally!)
	for (int i = 0; i < instance->physicalDeviceMemoryProperties.memoryTypeCount; i++) {
		if (!(types & (1U << i)))
			continue;
		// Actually attempt the allocation.
		pAllocate.memoryTypeIndex = i;
		VkDeviceMemory vmMemory = VK_NULL_HANDLE;
		if (vkAllocateMemory(instance->device, &pAllocate, NULL, &vmMemory) == VK_SUCCESS) {
			// printf("Alloc debug: %i\n", i);
			return vmMemory;
		}
	}
	return VK_NULL_HANDLE;
}

// This is the 'outer allocator', choosing which set of types to search based on requirements.
static VkDeviceMemory BTPOV_AllocateDeviceMemoryOverBufferMode(BTPOV_Instance * instance, VkDeviceSize size, uint32_t types, BTPOV_BufferMode mode) {
	// printf("Alloc BM: %i\n", mode);
	uint32_t optimalMask = BTPOV_GetMemoryTypesForBufferMode(instance, mode, 1);
	uint32_t subOptimalMask = BTPOV_GetMemoryTypesForBufferMode(instance, mode, 0);
	uint32_t heapMask = ~0U;
	// For an explaination, see description of BTPOV_CREATEINSTANCE_FLAGS_DONT_HSA_CHECK
	if (instance->hsaAvoidThisHeap != -1) {
		// printf("Alloc HSA Avoidance: %i\n", instance->hsaAvoidThisHeap);
		heapMask = ~BTPOV_GetMemoryTypesForHeap(&instance->physicalDeviceMemoryProperties, instance->hsaAvoidThisHeap);
	}
#define BTPOV_ADMOBM_ATTEMPTS 4
	uint32_t attempts[] = {
		types & optimalMask & heapMask,
		types & subOptimalMask & heapMask,
		types & optimalMask,
		types & subOptimalMask
	};
	uint32_t soFarMask = ~0U;
	for (int i = 0; i < BTPOV_ADMOBM_ATTEMPTS; i++) {
		VkDeviceMemory mem = BTPOV_AllocateDeviceMemoryOverTypeField(instance, size, attempts[i] & soFarMask);
		if (mem != VK_NULL_HANDLE)
			return mem;
		soFarMask &= ~attempts[i];
	}
	return VK_NULL_HANDLE;
}

BTPOV_Buffer * BTPOV_CreateBuffer(BTPOV_Instance * instance, VkDeviceSize size, BTPOV_BufferMode mode) {
	// Before doing anything else, check this
	if (size <= 0) {
		BTPOV_ReportError("BTPOV_CreateBuffer: Attempted to allocate buffer of <= 0 size (VUID-VkBufferCreateInfo-size-00912)");
		return NULL;
	}
	// Create the handle for a buffer.
	VkBufferCreateInfo pCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = size,
		.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT
	};
	VkBuffer vmBuffer = VK_NULL_HANDLE;
	if (vkCreateBuffer(instance->device, &pCreateInfo, NULL, &vmBuffer) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateBuffer: Could not create VkBuffer.");
		return NULL;
	}
	// Get memory requirements. Oh dear goodness.
	VkMemoryRequirements memoryRequirements;
	vkGetBufferMemoryRequirements(instance->device, vmBuffer, &memoryRequirements);
	// Now allocate the memory.
	VkDeviceMemory vmMemory = BTPOV_AllocateDeviceMemoryOverBufferMode(instance, memoryRequirements.size, memoryRequirements.memoryTypeBits, mode);
	if (vmMemory == VK_NULL_HANDLE) {
		BTPOV_ReportError("BTPOV_CreateBuffer: Could not allocate VkDeviceMemory (out of memory in all possible heaps)");
		goto hasBufferAndError;
	}
	if (vkBindBufferMemory(instance->device, vmBuffer, vmMemory, 0) != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_CreateBuffer: Could not bind buffer to memory.");
		goto hasMemoryAndError;
	}
	BTPOV_Buffer * buffer = malloc(sizeof(BTPOV_Buffer));
	if (buffer) {
		buffer->instance = instance;
		buffer->buffer = vmBuffer;
		buffer->memory = vmMemory;
		buffer->mode = mode;
		buffer->map = NULL;
	} else {
		BTPOV_ReportError("BTPOV_CreateBuffer: Could not malloc API structure.");
		hasMemoryAndError:
		vkFreeMemory(instance->device, buffer->memory, NULL);
		hasBufferAndError:
		vkDestroyBuffer(instance->device, buffer->buffer, NULL);
		buffer = NULL;
	}
	return buffer;
}

void BTPOV_DestroyBuffer(BTPOV_Buffer * buffer) {
	BTPOV_UnmapBuffer(buffer);
	vkDestroyBuffer(buffer->instance->device, buffer->buffer, NULL);
	vkFreeMemory(buffer->instance->device, buffer->memory, NULL);
	free(buffer);
}

void * BTPOV_MapBuffer(BTPOV_Buffer * buffer) {
	if (buffer->mode == BTPOV_BUFFERMODE_GPU_ONLY) {
		BTPOV_ReportError("BTPOV_MapBuffer: Buffer is not supposed to be mappable.");
		return NULL;
	}
	if (!buffer->map)
		vkMapMemory(buffer->instance->device, buffer->memory, 0, VK_WHOLE_SIZE, 0, &(buffer->map));
	if (!buffer->map)
		BTPOV_ReportError("BTPOV_MapBuffer: Could not map buffer.");
	return buffer->map;
}

void BTPOV_UnmapBuffer(BTPOV_Buffer * buffer) {
	if (buffer->map) {
		vkUnmapMemory(buffer->instance->device, buffer->memory);
		buffer->map = NULL;
	}
}

void BTPOV_FlushBufferRead(BTPOV_Buffer * buffer) {
	VkMappedMemoryRange range = {
		.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
		.memory = buffer->memory,
		.size = VK_WHOLE_SIZE
	};
	vkInvalidateMappedMemoryRanges(buffer->instance->device, 1, &range);
}

void BTPOV_FlushBufferWrite(BTPOV_Buffer * buffer) {
	VkMappedMemoryRange range = {
		.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
		.memory = buffer->memory,
		.size = VK_WHOLE_SIZE
	};
	vkFlushMappedMemoryRanges(buffer->instance->device, 1, &range);
}

