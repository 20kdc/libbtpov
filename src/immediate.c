#include "btpov_internal.h"

VkResult BTPOV_ExecuteProgram(BTPOV_Program * program, BTPOV_Buffer ** buffers, uint32_t x, uint32_t y, uint32_t z) {
	BTPOV_ShaderModule * sm = program->bModule;
	BTPOV_Instance * instance = sm->instance;
	VkResult error = VK_SUCCESS;

	VkDescriptorPoolSize sz = {
		.descriptorCount = sm->bufferCount,
		.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
	};
	VkDescriptorPoolCreateInfo pci = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.maxSets = 1,
		.poolSizeCount = 1,
		.pPoolSizes = &sz
	};
	VkDescriptorPool vmDescriptorPool = VK_NULL_HANDLE;
	error = vkCreateDescriptorPool(instance->device, &pci, NULL, &vmDescriptorPool);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_ExecuteProgram: Failed to create descriptor pool.");
		return BTPOV_TransferErrorToRecording(instance, error);
	}
	VkDescriptorSetAllocateInfo ai = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.descriptorPool = vmDescriptorPool,
		.descriptorSetCount = 1,
		.pSetLayouts = &sm->descriptorSetLayout,
	};
	VkDescriptorSet vmDescriptorSet = VK_NULL_HANDLE;
	error = vkAllocateDescriptorSets(instance->device, &ai, &vmDescriptorSet);
	if (error != VK_SUCCESS) {
		BTPOV_ReportError("BTPOV_ExecuteProgram: Failed to allocate descriptor set.");
		vkDestroyDescriptorPool(instance->device, vmDescriptorPool, NULL);
		return BTPOV_TransferErrorToRecording(instance, error);
	}
	for (size_t i = 0; i < sm->bufferCount; i++) {
		VkDescriptorBufferInfo bufferInfo = {
			.buffer = buffers[i]->buffer,
			.offset = 0,
			.range = VK_WHOLE_SIZE
		};
		VkWriteDescriptorSet write = {
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
			.dstSet = vmDescriptorSet,
			.dstBinding = i,
			.dstArrayElement = 0,
			.descriptorCount = 1,
			.pBufferInfo = &bufferInfo
		};
		vkUpdateDescriptorSets(instance->device, 1, &write, 0, NULL);
	}

	BTPOV_OpenedCommand cmd = {
		.descriptorPool = vmDescriptorPool
	};
	if (BTPOV_OpenCommand(instance, &cmd)) {
		vkCmdBindPipeline(cmd.cmdBuf, VK_PIPELINE_BIND_POINT_COMPUTE, program->pipeline);
		vkCmdBindDescriptorSets(cmd.cmdBuf, VK_PIPELINE_BIND_POINT_COMPUTE, sm->pipelineLayout, 0, 1, &vmDescriptorSet, 0, NULL);
		vkCmdDispatch(cmd.cmdBuf, x, y, z);
	}
	return BTPOV_CloseCommand(&cmd);
}

VkResult BTPOV_ExecuteBufferCopy(BTPOV_Buffer * src, BTPOV_Buffer * dst, uint32_t regionCount, const VkBufferCopy* pRegions) {
	BTPOV_OpenedCommand cmd = {};
	if (BTPOV_OpenCommand(src->instance, &cmd))
		vkCmdCopyBuffer(cmd.cmdBuf, src->buffer, dst->buffer, regionCount, pRegions);
	return BTPOV_CloseCommand(&cmd);
}

VkResult BTPOV_ExecuteBufferUpdate(BTPOV_Buffer * dst, VkDeviceSize ofs, VkDeviceSize size, const void * data) {
	BTPOV_OpenedCommand cmd = {};
	if (BTPOV_OpenCommand(dst->instance, &cmd))
		vkCmdUpdateBuffer(cmd.cmdBuf, dst->buffer, ofs, size, data);
	return BTPOV_CloseCommand(&cmd);
}

