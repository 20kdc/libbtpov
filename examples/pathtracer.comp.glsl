#version 430

#define SIZE 1536

layout(local_size_x = 8, local_size_y = 8) in;

layout(set = 0, binding = 0) buffer Framebuffer {
	int data[];
};

uint randomness;

uint permute(uint i) {
	// mixture of xorshift32 and secret
	i ^= i << 13;
	i ^= i >> 17;
	return ((i ^ (i << 5)) * 0xAC) + 11;
}

uint rand() {
	randomness = permute(randomness);
	return randomness;
}

float randf() {
	return float((rand() & 0xFFFF00) >> 8) / 65535.0;
}

void querySph(vec3 pos, uint oMat, inout uint mat, inout float dist, inout vec3 normal) {
	float myDist = length(pos) - 1.0;
	if (myDist < dist) {
		mat = oMat;
		dist = myDist;
		normal = normalize(pos);
	}
}

void query(vec3 pos, out uint mat, out float dist, out vec3 normal) {
	// default: light plane
	mat = 2;
	dist = 256.0 - pos.y;
	normal = vec3(0.0, -1.0, 0.0);

	// objects
	querySph(pos - vec3(-1.0, 0.0, 4.0), 1, mat, dist, normal);
	querySph(pos - vec3(0.0, -1.0, 4.5), 1, mat, dist, normal);
	querySph(pos - vec3(0.0, 1.0, 5.0), 1, mat, dist, normal);
	querySph(pos - vec3(1.0, 0.0, 5.5), 1, mat, dist, normal);
	querySph(pos - vec3(0.0, 0.0, 6.0), 1, mat, dist, normal);
}

void main() {
	uvec2 addr = gl_GlobalInvocationID.xy;
	uint addrInt = addr.x + (addr.y * SIZE);
	randomness = permute(permute(permute(permute(addrInt))));
	randomness = permute(permute(permute(randomness)));
	// tracing logic vars
	vec3 tracePos = vec3(0.0, 0.0, 0.0);
	vec3 traceVel = normalize(vec3((vec2(float(addr.x), float(addr.y)) / float(SIZE)) - 0.5, 1.0));
	// result vars
	float result = 0.0; // final hit if any
	float resultMul = 1.0; // amount lost
	// main
	for (uint iter = 0; iter < 128; iter++) {
		uint mat;
		float dist;
		vec3 normal;
		query(tracePos, mat, dist, normal);
		if (dist < -0.05) {
			tracePos += traceVel * dist;
			if (mat == 2) {
				// hit light, immediate stop
				result = 1.0;
				break;
			} else if (mat == 1) {
				// hit scene, work out what to do here
				resultMul *= 0.9;
				traceVel = normalize(vec3(randf(), randf(), randf()) - 0.5);
				if (dot(traceVel, normal) < 0.0)
					traceVel = reflect(traceVel, normal);
			}
		} else {
			tracePos += traceVel * max(dist, 0.05);
		}
	}
	// output
	result *= resultMul;
	// gamma
	result = pow(result, 2.2);
	// finally
	data[addrInt] = min(max(int(result * 255), 0), 255);
}

