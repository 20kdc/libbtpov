#include <libbtpov.h>
#include <stdio.h>

void main() {
	BTPOV_Instance * instance = BTPOV_CreateInstanceAutomatic(0, NULL);

	BTPOV_Buffer * buffer = BTPOV_CreateBuffer(instance, 4, BTPOV_BUFFERMODE_CPU);

	int * bufferMap = BTPOV_MapBuffer(buffer);
	bufferMap[0] = 0;
	BTPOV_FlushBufferWrite(buffer);

	uint32_t newValueToSet = 1234;
	VkSpecializationMapEntry mapEntry = {
		.constantID = 0,
		.offset = 0,
		.size = sizeof(newValueToSet)
	};
	VkSpecializationInfo spec = {
		.mapEntryCount = 1,
		.pMapEntries = &mapEntry,
		.dataSize = sizeof(newValueToSet),
		.pData = &newValueToSet
	};
	BTPOV_ProgramDetails2 programDetails = {
		.bufferCount = 1,
		.name = "main",
		.specializationInfo = &spec
	};
	BTPOV_Program * program = BTPOV_CreateProgramFromFile2(instance, "simplespecialized.spv", &programDetails);

	BTPOV_ExecuteProgram(program, &buffer, 1, 1, 1);

	BTPOV_FlushBufferRead(buffer);
	printf("Resulting value: %i\n", bufferMap[0]);

	BTPOV_DestroyProgram(program);
	BTPOV_DestroyBuffer(buffer);
	BTPOV_DestroyInstance(instance);
}

