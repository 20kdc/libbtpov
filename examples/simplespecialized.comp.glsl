#version 430

layout(local_size_x = 1, local_size_y = 1) in;

// Notice how this default value is *not* 1234.
// This value is specialized into being 1234.
layout (constant_id = 0) const uint new_value = 0;

layout(set = 0, binding = 0) buffer IgnoreMeIAmAFish {
	uint data[];
} buf[1];

void main() {
	buf[0].data[0] = new_value;
}

