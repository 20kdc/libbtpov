#version 430

layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

layout(set = 0, binding = 0) buffer IgnoreMe {
	readonly restrict uint target;
	readonly restrict uint base;
};

layout(set = 0, binding = 1) buffer IgnoreMeAsWell {
	volatile restrict uint result;
	volatile restrict uint resultFound;
	volatile restrict uint checks;
};

layout(set = 0, binding = 2) buffer IgnoreMeToo {
	readonly restrict uint table[256];
};

uint applyCRC(uint hash, uint byte) {
	uint index = (hash & 0xFFU) ^ byte;
	return (hash >> 8U) ^ table[index];
}

uint applyCRC32(uint value) {
	uint crc1 = applyCRC(0xFFFFFFFFU, (value >> 24) & 0xFF);
	uint crc2 = applyCRC(crc1, (value >> 16) & 0xFF);
	uint crc3 = applyCRC(crc2, (value >> 8) & 0xFF);
	uint crc4 = applyCRC(crc3, (value >> 0) & 0xFF);
	return crc4 ^ 0xFFFFFFFFU;
}

void main() {
	uint value = base + (gl_LocalInvocationID.x + (((gl_WorkGroupID.x * 64) + gl_WorkGroupID.y) * 128));
	uint hash = applyCRC32(value);
	if (hash == target) {
		atomicExchange(result, value);
		atomicExchange(resultFound, 1);
	}
	atomicAdd(checks, 1);
}

