#include <libbtpov.h>
#include <stdio.h>

void main() {
	BTPOV_Instance * instance = BTPOV_CreateInstanceAutomatic(0, NULL);
	if (!instance) {
		puts("Unable to find a device.");
		return;
	}

	VkPhysicalDeviceProperties props;	
	BTPOV_GetInstancePhysicalDeviceProperties(instance, &props);

	printf("BTPOV will use this device by default:\n");
	printf("Name: %s\n", props.deviceName);
	printf("Type: %x\n", props.deviceType);

	printf("Memory Preferences:\n");
	for (int i = 0; i < BTPOV_BUFFERMODE_MAX; i++) {
		printf(" %i O :", i);
		uint32_t types = BTPOV_GetMemoryTypesForBufferMode(instance, i, 1);
		for (int i = 0; i < 32; i++) {
			if (!(types & (1U << i)))
				continue;
			printf(" %02i", i);
		}
		printf("\n %i S :", i);
		types = BTPOV_GetMemoryTypesForBufferMode(instance, i, 0);
		for (int i = 0; i < 32; i++) {
			if (!(types & (1U << i)))
				continue;
			printf(" %02i", i);
		}
		printf("\n");
	}

	BTPOV_DestroyInstance(instance);
}

