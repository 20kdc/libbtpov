#include <libbtpov.h>
#include <stdio.h>

void main() {
	BTPOV_Instance * instance = BTPOV_CreateInstanceAutomatic(0, NULL);

	BTPOV_Buffer * buffer = BTPOV_CreateBuffer(instance, 8, BTPOV_BUFFERMODE_CPU);

	int * bufferMap = BTPOV_MapBuffer(buffer);

	int tmp = 1234;
	BTPOV_ExecuteBufferUpdate(buffer, 0, 4, &tmp);

	tmp = 0;
	BTPOV_ExecuteBufferUpdate(buffer, 4, 4, &tmp);

	BTPOV_FlushBufferRead(buffer);
	printf("first (should be 1234): %i\n", bufferMap[0]);
	printf("second (should be 0): %i\n", bufferMap[1]);

	VkBufferCopy copy = {
		.srcOffset = 0,
		.dstOffset = 4,
		.size = 4
	};
	BTPOV_ExecuteBufferCopy(buffer, buffer, 1, &copy);

	BTPOV_FlushBufferRead(buffer);
	printf("second after copy (should be 1234): %i\n", bufferMap[1]);

	BTPOV_DestroyBuffer(buffer);
	BTPOV_DestroyInstance(instance);
}

