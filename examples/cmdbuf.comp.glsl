#version 430

layout(local_size_x = 1, local_size_y = 1) in;

layout(set = 0, binding = 0) buffer IgnoreMeIAmAFish {
	uint test;
};

void main() {
	atomicAdd(test, 1);
}

