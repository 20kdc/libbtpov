#include <libbtpov.h>
#include <stdio.h>

// must be dividable by 8 for workgroup reasons
// (SIZE / 8) squared cannot be >= 65536
#define SIZE 1536

void main() {
	BTPOV_Instance * instance = BTPOV_CreateInstanceAutomatic(0, NULL);

	BTPOV_Buffer * buffer = BTPOV_CreateBuffer(instance, sizeof(uint32_t) * SIZE * SIZE, BTPOV_BUFFERMODE_CPU);

	uint32_t * bufferMap = BTPOV_MapBuffer(buffer);

	BTPOV_ProgramDetails programDetails = {
		.bufferCount = 1,
		.name = "main"
	};
	BTPOV_Program * program = BTPOV_CreateProgramFromFile(instance, "pathtracer.spv", &programDetails);

	fprintf(stderr, "executing program...\n");
	BTPOV_ExecuteProgram(program, &buffer, SIZE >> 3, SIZE >> 3, 1);
	fprintf(stderr, "done, writing PPM...\n");

	BTPOV_FlushBufferRead(buffer);
	printf("P3\n");
	printf("%i %i\n", SIZE, SIZE);
	printf("255\n");
	for (int i = 0; i < (SIZE * SIZE); i++) {
		uint8_t v = bufferMap[i];
		printf("%i %i %i\n", v, v, v);
	}

	BTPOV_DestroyProgram(program);
	BTPOV_DestroyBuffer(buffer);
	BTPOV_DestroyInstance(instance);
}

