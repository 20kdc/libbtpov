#include <libbtpov.h>
#include <stdio.h>

void main() {
	BTPOV_Instance * instance = BTPOV_CreateInstanceAutomatic(0, NULL);

	BTPOV_Buffer * buffer = BTPOV_CreateBuffer(instance, 4, BTPOV_BUFFERMODE_CPU);

	int * bufferMap = BTPOV_MapBuffer(buffer);
	bufferMap[0] = 0;
	BTPOV_FlushBufferWrite(buffer);

	BTPOV_ProgramDetails programDetails = {
		.bufferCount = 1,
		.name = "main"
	};
	BTPOV_Program * program = BTPOV_CreateProgramFromFile(instance, "cmdbuf.spv", &programDetails);

	BTPOV_BeginCommandBuffer(instance);

	BTPOV_ExecuteProgram(program, &buffer, 1, 1, 1);
	BTPOV_ExecuteProgram(program, &buffer, 1, 1, 1);
	BTPOV_ExecuteProgram(program, &buffer, 1, 1, 1);

	BTPOV_CommandBuffer * cmdbuf = BTPOV_EndCommandBuffer(instance);

	BTPOV_FlushBufferRead(buffer);
	printf("This should be 0: %i\n", bufferMap[0]);

	BTPOV_ExecuteCommandBuffer(cmdbuf);

	BTPOV_FlushBufferRead(buffer);
	printf("This should be 3: %i\n", bufferMap[0]);

	BTPOV_DestroyCommandBuffer(cmdbuf);
	BTPOV_DestroyProgram(program);
	BTPOV_DestroyBuffer(buffer);
	BTPOV_DestroyInstance(instance);
}

