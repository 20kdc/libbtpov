#include <libbtpov.h>
#include <stdio.h>

static BTPOV_Buffer * genPoly(BTPOV_Instance * instance) {
	BTPOV_Buffer * polyBuffer = BTPOV_CreateBuffer(instance, 1024, BTPOV_BUFFERMODE_GPU_ONLY);

	BTPOV_ProgramDetails programDetails = {
		.bufferCount = 1,
		.name = "main"
	};
	BTPOV_Program * program = BTPOV_CreateProgramFromFile(instance, "findcrc.poly.spv", &programDetails);
	BTPOV_ExecuteProgram(program, &polyBuffer, 2, 1, 1);
	BTPOV_DestroyProgram(program);

	return polyBuffer;
}

int main(int argc, char ** argv) {
	if (argc != 2) {
		puts("findcrc VALUE");
		puts("finds 4 bytes where the resulting CRC32 (of reversed polynomial 0xEDB88320U) equals VALUE");
		return 1;
	}
	// Parse value

	int value = 0;
	sscanf(argv[1], "%i", &value);

	// Initialization

	BTPOV_Instance * instance = BTPOV_CreateInstanceAutomatic(0, NULL);

	BTPOV_Buffer * targetBuffer = BTPOV_CreateBuffer(instance, 8, BTPOV_BUFFERMODE_GPU);
	BTPOV_Buffer * resultBuffer = BTPOV_CreateBuffer(instance, 12, BTPOV_BUFFERMODE_CPU);
	BTPOV_Buffer * polyBuffer = genPoly(instance);

	BTPOV_Buffer * buffers[] = {targetBuffer, resultBuffer, polyBuffer};

	uint32_t * targetBufferMap = BTPOV_MapBuffer(targetBuffer);
	targetBufferMap[0] = value;
	targetBufferMap[1] = 0;
	BTPOV_FlushBufferWrite(targetBuffer);

	uint32_t * resultBufferMap = BTPOV_MapBuffer(resultBuffer);
	resultBufferMap[0] = 0;
	resultBufferMap[1] = 0;
	resultBufferMap[2] = 0;
	BTPOV_FlushBufferWrite(resultBuffer);

	BTPOV_ProgramDetails programDetails = {
		.bufferCount = 3,
		.name = "main"
	};
	BTPOV_Program * program = BTPOV_CreateProgramFromFile(instance, "findcrc.core.spv", &programDetails);

	uint32_t groupX = 8192;
	uint32_t groupY = 64;
	uint32_t checksPerGroup = groupX * groupY * 128;
	uint32_t checksTally = 0;

	int ok = 0;
	while (1) {
		BTPOV_ExecuteProgram(program, buffers, groupX, groupY, 1);
		// update checks tally and base for next round
		checksTally += checksPerGroup;
		// printf("%08x\n", targetBufferMap[1]);
		BTPOV_FlushBufferRead(resultBuffer);
		uint32_t checksActual = resultBufferMap[2];
		if (checksActual != checksTally) {
			printf("Error: Expected checks = %x, found %x\n", checksTally, checksActual);
			break;
		}
		if (resultBufferMap[1]) {
			ok = 1;
			printf("%X -> %X\n", value, resultBufferMap[0]);
			break;
		}
		// update base for next round
		targetBufferMap[1] += checksPerGroup;
		BTPOV_FlushBufferWrite(targetBuffer);
		if (targetBufferMap[1] == 0)
			break;
	}

	if (!ok)
		printf("No result found.\n");

	BTPOV_DestroyProgram(program);
	BTPOV_DestroyBuffer(polyBuffer);
	BTPOV_DestroyBuffer(targetBuffer);
	BTPOV_DestroyBuffer(resultBuffer);
	BTPOV_DestroyInstance(instance);

	return 0;
}

