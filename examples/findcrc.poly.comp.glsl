#version 430

layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

layout(set = 0, binding = 0) buffer IgnoreMeAsWell {
	volatile restrict uint table[256];
};

void main() {
	uint modbyte = gl_LocalInvocationID.x + (gl_WorkGroupID.x * 128);
	uint value = modbyte;
	for (uint j = 0; j < 8; j++)
		value = (value >> 1) ^ ((value & 1U) * 0xEDB88320U);
	table[modbyte] = value;
}

